function Hf=hessianTensorInt_biologicalModel(x,u)



 Hf{1} = interval(sparse(8,8),sparse(8,8));

Hf{1}(6,1) = -1;
Hf{1}(1,6) = -1;


 Hf{2} = interval(sparse(8,8),sparse(8,8));



 Hf{3} = interval(sparse(8,8),sparse(8,8));

Hf{3}(4,3) = -5;
Hf{3}(3,4) = -5;


 Hf{4} = interval(sparse(8,8),sparse(8,8));

Hf{4}(4,3) = -5;
Hf{4}(3,4) = -5;
Hf{4}(6,5) = 5;
Hf{4}(5,6) = 5;


 Hf{5} = interval(sparse(8,8),sparse(8,8));

Hf{5}(4,3) = 5;
Hf{5}(3,4) = 5;
Hf{5}(6,5) = -5;
Hf{5}(5,6) = -5;


 Hf{6} = interval(sparse(8,8),sparse(8,8));

Hf{6}(6,5) = -5;
Hf{6}(5,6) = -5;


 Hf{7} = interval(sparse(8,8),sparse(8,8));

Hf{7}(6,5) = 5;
Hf{7}(5,6) = 5;
