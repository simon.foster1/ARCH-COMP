function Hf=hessianTensor_brusselator(x,u)



 Hf{1} = sparse(3,3);

Hf{1}(1,1) = 2*x(2);
Hf{1}(2,1) = 2*x(1);
Hf{1}(1,2) = 2*x(1);


 Hf{2} = sparse(3,3);

Hf{2}(1,1) = -2*x(2);
Hf{2}(2,1) = -2*x(1);
Hf{2}(1,2) = -2*x(1);
