function int = interval(obj)
% interval - compute and interval enclosure of a strips object
%
% Syntax:  
%    int = interval(obj)
%
% Inputs:
%    obj - strips object
%
% Outputs:
%    int - interval object
%
% Example: 
%    C = [1 1; 1 -2];
%    d = [1; 0.5];
%    y = [1; 0];       
%    S = strips(C,d,y);
%
%    int = interval(S);
%   
%    xlim([-5,5]); ylim([-5,5]);
%    plot(S,[1,2],'r');
%    plot(int,[1,2],'b');
%    xlim([-4,4]); ylim([-4,4]);
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: strips

% Author:       Niklas Kochdumper
% Written:      23-November-2020
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

    if size(obj.C,1) < dim(obj)
       error('Interval enclosure is unbounded!'); 
    end

    % convert to polytope
    poly = mptPolytope(obj);
    
    % convert interval enclosure
    int = interval(poly);
    
%------------- END OF CODE --------------