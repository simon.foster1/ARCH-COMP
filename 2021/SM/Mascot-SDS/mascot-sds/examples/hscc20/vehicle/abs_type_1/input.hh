/* Header file for inputs */
/* Created by: Kaushik */
/*      Date: 16/10/2019 */

#ifndef INPUT_HH_
#define INPUT_HH_

/* state space dim */
#define sDIM 3
#define iDIM 1

/* data types for the ode solver */
typedef std::array<double,sDIM> state_type;
typedef std::array<double,iDIM> input_type;

/* sampling time */
const double tau = 1;
/* upper bounds on the noise which is considered to be centered around the origin */
const state_type W_ub = {0.06,0.06,0.06};
/* velocity */
const double V = 0.1;

/* parallel or sequential implementation of computation of the
transition functions */
const bool parallel = false;
/* read transitions from file flag */
const bool RFF = false;

auto decomposition = [](input_type &u, state_type &z1, state_type &z2) -> state_type {
  /* first convert z1[2] to be within -pi and +pi */
  if (z1[2]>M_PI) {
    double rem = fmod(z1[2],M_PI);
    z1[2] = -M_PI + rem;
  }
  if (z1[2]<-M_PI) {
    double rem = fmod(-z1[2],M_PI);
    z1[2] = M_PI - rem;
  }
    auto rhs0 = [](double x3, input_type& u) -> double {
        double r;
        if (u[0]!=0) {
            r = V*sin(x3+u[0]*tau)/u[0] - V*sin(x3)/u[0];
        } else {
            r = V*cos(x3)*tau;
        }
        return r;
    };
    auto rhs1 = [](double x3, input_type& u) -> double {
        double r;
        if (u[0]!=0) {
            r = - V*cos(x3+u[0]*tau)/u[0] + V*cos(x3)/u[0];
        } else {
            r = - V*sin(x3)*tau;
        }
        return r;
    };
    std::vector<double> list0, list1;
    list0.push_back(z1[0]+rhs0(z1[2],u));
    list0.push_back(z1[0]+rhs0(z2[2],u));
    list0.push_back(z2[0]+rhs0(z1[2],u));
    list0.push_back(z2[0]+rhs0(z2[2],u));
    
    list1.push_back(z1[1]+rhs1(z1[2],u));
    list1.push_back(z1[1]+rhs1(z2[2],u));
    list1.push_back(z2[1]+rhs1(z1[2],u));
    list1.push_back(z2[1]+rhs1(z2[2],u));
    if (u[0]!=0) {
        double alpha0 = atan2((cos(u[0]*tau)-1),sin(u[0]*tau));
        if (alpha0>=z1[2] && alpha0<=z2[2] |
            alpha0<=z1[2] && alpha0>=z2[2]) {
            list0.push_back(z1[0]+rhs0(alpha0,u));
        }
        double alpha1 = atan2(sin(u[0]*tau),(1-cos(u[0]*tau)));
        if (alpha1>=z1[2] && alpha1<=z2[2] |
            alpha1<=z1[2] && alpha1>=z2[2]) {
            list1.push_back(z1[1]+rhs1(alpha1,u));
        }
    } else {
        double z2_min = std::min(z1[2],z2[2]);
        double z2_max = std::max(z1[2],z2[2]);
        if (0>=z2_min && 0<=z2_max) {
            list0.push_back(z1[0]+V*tau);
            list1.push_back(z1[1]);
        }
        if (-M_PI>=z2_min && -M_PI<=z2_max |
            M_PI>=z2_min && M_PI<=z2_max) {
            list0.push_back(z1[0]-V*tau);
            list1.push_back(z1[1]);
        }
        /* extremum occurs for cos(x[2])=0 */
        if (M_PI/2>=z2_min && M_PI/2<=z2_max) {
            list0.push_back(z1[0]);
            list1.push_back(z1[1]-V*tau);
        }
        if (-M_PI/2>=z2_min && -M_PI/2<=z2_max) {
            list0.push_back(z1[0]);
            list1.push_back(z1[1]+V*tau);
        }
    }
  state_type y;
    if (z1[0]<z2[0]) { /* z1 is the lower bound and z2 is the upper bound */
        /* return the lower left corner of the reachable set approximation */
        y[0] = *std::min_element(std::begin(list0),std::end(list0));
        y[1] = *std::min_element(std::begin(list1),std::end(list1));
        y[2] = z1[2] + u[0]*tau;
    } else { /* z1 is the upper bound and z2 is the lower bound */
        /* return the upper right corner of the reachable set approximation */
        y[0] = *std::max_element(std::begin(list0),std::end(list0));
        y[1] = *std::max_element(std::begin(list1),std::end(list1));
        y[2] = z1[2] + u[0]*tau;
    }
  return y;
};

/* there is an intentional overlap between the two ends in the 3rd dimension to allow out-of-bound behavior */
/* IMPORTANT: the overlap amount is ad-hoc and needs to be updated with eta */
/* lower bounds of the hyper rectangle */
double lb[sDIM]={0.0,0.0,-M_PI-0.5};
/* upper bounds of the hyper rectangle */
double ub[sDIM]={2.0,3.0,M_PI+0.5};
/* grid node distance diameter */
double eta[sDIM]={0.1,0.1,0.1};

/* input space parameters */
double ilb[iDIM]={-1.0};
double iub[iDIM]={1.0};
double ieta[iDIM]={0.1};

/* target states */
/* add inner approximation of P={ x | H x<= h } form state space */
double H[4*sDIM]={-1, 0, 0,
                   1, 0, 0,
                   0,-1, 0,
                   0, 1, 0};
double h[4] = {-0.8,1.2,-0.2,0.6};

/* obstacle set */
double HO[4*sDIM]={-1, 0, 0,
                   1, 0, 0,
                   0,-1, 0,
                   0, 1, 0};
double ho1[4] = {-0.8,1.2,-1.0,1.4};

#endif /* INPUT_HH_ */
