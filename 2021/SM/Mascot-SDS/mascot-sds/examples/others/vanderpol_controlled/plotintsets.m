addpath(genpath('../../../'))
% state space bounds 
[sDIM,~,~,lb,ub,~] = readParams('input.hh');

% prepare the figure window
figure;
axis(reshape([lb'; ub'],[1 2*sDIM]));
hold on;
plot(-3.4,5.8,'ko')
for ii=1:109
    set = SymbolicSet(['IntSetsUnder#1/P9/Q' num2str(ii) '.bdd']);
    p = set.points;
    plot(p(:,1),p(:,2),'.');
    pause
end