function Hf=hessianTensorInt_brusselatorShift(x,u)



 Hf{1} = interval(sparse(3,3),sparse(3,3));

Hf{1}(1,1) = 2*x(2) + 19/10;
Hf{1}(2,1) = 2*x(1) + 19/10;
Hf{1}(1,2) = 2*x(1) + 19/10;


 Hf{2} = interval(sparse(3,3),sparse(3,3));

Hf{2}(1,1) = - 2*x(2) - 19/10;
Hf{2}(2,1) = - 2*x(1) - 19/10;
Hf{2}(1,2) = - 2*x(1) - 19/10;
