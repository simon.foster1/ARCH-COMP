function han = plot(obj,varargin)
% plot - Plots 2-dimensional projection of a halfspace
%
% Syntax:  
%    han = plot(obj)
%    han = plot(obj,dim,color)
%
% Inputs:
%    obj - halfspace object
%    dim - dimensions that should be projected (optional); assume
%          that other entries of the normal vector are zeros
%    color - color for the plot
%
% Outputs:
%    han - handle to the plotted object
%
% Example: 
%    hs = halfspace([1;1],0);
%
%    xlim([-4,4]);
%    ylim([-4,4]);
%    plot(hs,[1,2],'r');
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: conHyperplane/plot

% Author:       Matthias Althoff, Niklas Kochdumper
% Written:      23-August-2013
% Last update:  19-November-2019 (NK, plot area instead of line)
% Last revision:---

%------------- BEGIN CODE --------------

    % parse input arguments
    dim = [1,2];
    color = 'b';

    if nargin >= 2 && ~isempty(varargin{1})
        dim = varargin{1};
    end

    if nargin >= 3 && ~isempty(varargin{2})
        color = varargin{2}; 
    end

    % get size of current plot
    xLim = get(gca,'Xlim');
    yLim = get(gca,'Ylim');

    % convert to mptPolytope
    C = [obj.c(dim)';eye(2);-eye(2)];
    d = [obj.d;xLim(2);yLim(2);-xLim(1);-yLim(1)];

    poly = mptPolytope(C,d);

    % plot mptPolytope
    han = plotFilled(poly,dim,color,varargin{3:end});

%------------- END OF CODE --------------