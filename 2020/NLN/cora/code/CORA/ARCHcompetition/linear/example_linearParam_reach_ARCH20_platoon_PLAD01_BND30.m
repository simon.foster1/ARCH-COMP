function example_linearParam_reach_ARCH20_platoon_PLAD01_BND30()
% example_linearParam_reach_ARCH20_platoon_PLAD01_BND30 -  example of linear
% reachability analysis from the ARCH20 friendly competition (platoon example);
% the linear dynamics can switch arbitrarily
%
% Syntax:  
%    example_linearParam_reach_ARCH20_platoon_PLAD01_BND30
%
% Inputs:
%    no
%
% Outputs:
%    res - boolean 
%
% Example: 
%
% 
% Author:       Matthias Althoff
% Written:      05-April-2017
% Last update:  13-March-2019
% Last revision:---

%------------- BEGIN CODE --------------


        
% Parameter ---------------------------------------------------------------

% initial set
R0 = zonotope(zeros(9,1));
Rtp{1} = R0;

% uncertain inputs
params.U = zonotope(interval(-9,1));

% final time
params.tFinal = 5;


% Reachability Settings ---------------------------------------------------

options.timeStep = 0.02; 
options.zonotopeOrder = 200;
options.taylorTerms = 4;



% System Dynamics ---------------------------------------------------------

% controlled system
A_c = [...
        0    1.0000         0         0         0         0         0         0         0;...
        0         0   -1.0000         0         0         0         0         0         0;...
        1.6050    4.8680   -3.5754   -0.8198    0.4270   -0.0450   -0.1942    0.3626   -0.0946;...
        0         0         0         0    1.0000         0         0         0         0;...
        0         0    1.0000         0         0   -1.0000         0         0         0;...
        0.8718    3.8140   -0.0754    1.1936    3.6258   -3.2396   -0.5950    0.1294   -0.0796;...
        0         0         0         0         0         0         0    1.0000         0;...
        0         0         0         0         0    1.0000         0         0   -1.0000;...
        0.7132    3.5730   -0.0964    0.8472    3.2568   -0.0876    1.2726    3.0720   -3.1356 ]; 

% uncontrolled system
A_n = [...
        0    1.0000         0         0         0         0         0         0         0;...
        0         0   -1.0000         0         0         0         0         0         0;...
        1.6050    4.8680   -3.5754         0         0         0         0         0         0;...
        0         0         0         0    1.0000         0         0         0         0;...
        0         0    1.0000         0         0   -1.0000         0         0         0;...
        0         0         0    1.1936    3.6258   -3.2396         0         0         0;...
        0         0         0         0         0         0         0    1.0000         0;...
        0         0         0         0         0    1.0000         0         0   -1.0000;...
        0.7132    3.5730   -0.0964    0.8472    3.2568   -0.0876    1.2726    3.0720   -3.1356 ];   
    
    
B = [0 ; 1; 0; 0; 0; 0; 0; 0; 0 ];

% instantiate linear dynamics with constant parameters
linSys_c  = linearSys('c',A_c, B);
linSys_n  = linearSys('n',A_n, B);



% Reachability Analysis ---------------------------------------------------

tic
Rcont = cell(4,1);

% loop over all changes between controlled and uncontrolled system
for i = 1:2    
    % reachable set for controlled system
    params.R0 = Rtp{end};
    [Rcont{2*i-1},Rtp] = reach(linSys_c, params, options);
    
    % reachable set for uncnotrolled system
    params.R0 = Rtp{end};
    [Rcont{2*i},Rtp] = reach(linSys_n, params, options);
end

tComp = toc;



% Verification ------------------------------------------------------------

tic
violation30 = 0;

for iRun = 1:length(Rcont)
    for i = 1:length(Rcont{iRun})
        x_proj = interval(project(Rcont{iRun}{i},[1,4,7]));
        if any(infimum(x_proj) < -30)
            violation30 = 1;
        end
    end
end

tVer = toc;

disp(['spefication verified: ',num2str(~violation30)]);
disp(['computation time: ',num2str(tVer+tComp)]);



% Simulation --------------------------------------------------------------

% parameter for simulation
runs = 60;

simOpt.points = 1;
simOpt.fracVert = 0.5;
simOpt.fracInpVert = 1;
simOpt.inpChanges = 6;

% initial points
points = cell(runs,1);
simRes = cell(4*runs,1);

for i = 1:runs
    points{i} = randPoint(R0); 
end

% loop over all changes between controlled and uncontrolled system
t = 0;
counter = 1;

for i = 1:2
   
    % simulate controlled system
    if t ~= 0
        params.tStart = t;
    end
    params.tFinal = t + 5;
    t = t + 5;
    
    for j = 1:length(points)
        params.R0 = zonotope(points{j});
        temp = simulateRandom(linSys_c, params, simOpt);
        points{j} = temp.x{1}(end,:)';
        simRes{counter} = temp;
        counter = counter + 1;
    end
    
    % simulate uncontrolled system
    params.tStart = t;
    params.tFinal = t + 5;
    t = t + 5;
    
    for j = 1:length(points)
        params.R0 = zonotope(points{j});
        temp = simulateRandom(linSys_n, params, simOpt);
        points{j} = temp.x{1}(end,:)';
        simRes{counter} = temp;
        counter = counter + 1;
    end
end



% Visualization -----------------------------------------------------------

figure; hold on

% reachable set over time
t = 0;

for i = 1:length(Rcont)
    for j = 1:length(Rcont{i}) 

        % compute interval
        intX = interval(project(Rcont{i}{j},1));
        intT = interval(t,t+options.timeStep);
        int = cartProd(intT,intX);
        
        % plot interval
        plotFilled(int,[1 2],[.75 .75 .75],'EdgeColor','none');
        
        % update time
        t = t + options.timeStep;
    end
end

% plot simulation results
for i = 1:length(simRes)
    for j = 1:length(simRes{i}.t)
        plot(simRes{i}.t{j},simRes{i}.x{j}(:,1),'k');
    end
end

box on
xlabel('t');
ylabel('x_1');

%------------- END OF CODE --------------