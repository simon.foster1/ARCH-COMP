function [obj] = halfspace(obj)
% halfspace - Generates halfspace representation of the zonotope
%
% Syntax:  
%    [obj] = halfspace(obj)
%
% Inputs:
%    obj - zonotope object
%
% Outputs:
%    obj - zonotope object
%
% Example: 
%    ---
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: ---

% Author:       Matthias Althoff
% Written:      07-May-2007 
% Last update:  06-April-2017
%               16-October-2019: added box case (VG)
% Last revision:---

%------------- BEGIN CODE --------------
%check if zonotope is a parallelipiped
G = generators(obj);
if all(size(G)==size(G,1)) && rank(G)==size(G,1)
    c = center(obj);
    n = size(G,1);
    %x=c+G*u, |u|<=1 -> [I;-I]*inv(G)*(x-c)<=1
    A = [eye(n);-eye(n)];
    H = A*inv(G);
    K = ones(2*n,1)+H*c;
    %normalize H to H(i,:)=1
    hn = sqrt(sum(H.^2,2));
    H = H./repmat(hn,1,size(H,2));
    K = K./hn;
else
    %convert zonotope to polytope and retrieve halfspace representation
    P = polytope(obj);
    H = get(P,'H');
    K = get(P,'K');
end
%write to object structure
obj.halfspace.H=H;
obj.halfspace.K=K;
obj.halfspace.equations=length(K);

%------------- END OF CODE --------------