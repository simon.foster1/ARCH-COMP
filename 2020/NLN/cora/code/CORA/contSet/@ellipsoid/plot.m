function h=plot(varargin)
% plot - Plots 2-dimensional projection of an ellipsoid
%
% Syntax:  
%    h = plot(E) plots the ellipsoid E for the first two dimensions
%    h = plot(E,dims) plots the ellipsoid E for the two dimensions i,j: "dims=[i,j]" and returns handle to line-plot object
%    h = plot(E,dims,'Color','red',...) adds the standard plotting preferences
%
% Inputs:
%    E - ellipsoid object
%    dims - dimensions that should be projected (optional) 
%
% Outputs:
%    handle
%
% Example: 
%    E=ellipsoid([1 0 0; 0 1 0;0 0 3]);
%    plot(E)
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Victor Gassmann
% Written:      13-March-2019
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

%If only one argument is passed
if nargin==1
    E=varargin{1};
    dims=[1,2];
    type{1}='b';
    
%If two arguments are passed    
elseif nargin==2
    E=varargin{1};
    dims=varargin{2};
    type{1}='b';
    
%If three or more arguments are passed
elseif nargin>=3
    E=varargin{1};
    dims=varargin{2};   
    type(1:length(varargin)-2)=varargin(3:end);
end

N = 1000;
t = linspace(0,2*pi,N);
L = [cos(t);sin(t)];
if length(E.Q)==1%scalar
    dims = 1;
end
% project ellipsoid
E_p = project(E,dims);
%Since L only contains unit vectors l, we know that there exists a y such
%that l'*y = suppfnc(E_p,L)=l'*q+sqrt(l'*Q*l). Therefore, y =
%q+Q*l/sqrt(l'*Q*l).
if length(E.Q)==1
    Y = boundary(E,2);%N=2 here returns exactly the two boundary points
    h = plot(Y,zeros(size(Y)),type{:});
    set(gca,'ytick',[],'Ycolor','w','box','off')
    return;
end
Y = zeros(length(dims),size(L,2));
for i=1:size(L,2)
    l = L(:,i);
    Y(:,i) = E_p.q + E_p.Q*l/sqrt(l'*E_p.Q*l);
end
%plot and output the handle
h = plot(Y(1,:),Y(2,:),type{:});

%------------- END OF CODE --------------