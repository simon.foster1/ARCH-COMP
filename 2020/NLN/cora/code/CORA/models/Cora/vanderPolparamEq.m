function [dx]=vanderPolparamEq(x,u,p)

mu=1;
%mu=0.3;

dx(1,1)=p(1)*x(2);
dx(2,1)=mu*(1-x(1)^2)*x(2)*p(2)-x(1)+u(1);
