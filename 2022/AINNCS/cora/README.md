# ARCH-COMP 2022: AINNCS Category
## Tool: CORA
The COntinuous Reachability Analyzer (CORA) is a collection of MATLAB classes for the formal verification of cyber-physical systems using reachability analysis. 
More Information here: 
https://tumcps.github.io/CORA/

## Installation
This folder contains the code as well as a docker file to run the AINNCS benchmarks in one click.

However, you need to provide a MATLAB Licence file `license.lic` to run the code:
- Create a MATLAB License file: 
	For the docker container to run MATLAB, one has to create a new license file for the container.
	Log in with your MATLAB account at https://www.mathworks.com/licensecenter/licenses/
	Click on your license, and then navigate to
	1. "Install and Activate"
	1. "Activate to Retrieve License File"
	1. "Activate a Computer"
	(...may differ depending on how your licensing is set up).
- Choose:
	- Release: `R2022a`
	- Operating System: `Linux`
	- Host ID: `0242AC110002` (= Default MAC-Adress of Docker Container) | your host MAC Adress
	- Computer Login Name: `matlab`
	- Activation Label: `<any name>`
- Download the file and place it next to the docker file

If you are having trouble, don't hesitate contacting us: <a href="mailto:tobias.ladner@tum.de">tobias.ladner@tum.de</a>

## Run the Code

You can run all benchmarks in one click in a docker container using the `measure_all.sh` script.

	./measure_all.sh
	
The results will be stored to `./results`.

## Tool Comparisons and Plots

Note that the total time shown in `./results/results.txt` includes the time for verification as well as tries to violate a benchmark (see <a href="https://cps-vo.org/node/84803#comment-1301" target="_blank">forum discussion</a>), as shown in the example below.
For better comparison in the report, the times for each subtask is shown as well.
	
	BENCHMARK: Adaptive Cruise Controller (ACC)
	Time to compute random Simulations: 0.94224
	Time to check Violation in Simulations: 0.0030243
	Time to compute Reachable Set: 1.5869
	Time to check Verification: 0.049175
	Result: VERIFIED
	Total Time: 2.5813

Additionally, we provide the results from a reference run on our machine in `results_ref`.
This folder also contains plots for each benchmark in vector graphic format for the report.

## References

- [1] Althoff, M. (2015). An introduction to CORA 2015. In Proc. of the workshop on applied verification for continuous and hybrid systems (pp. 120-151).
- [2] Kochdumper, N., Schilling, C., Althoff, M., & Bak, S. (2022). Open-and Closed-Loop Neural Network Verification using Polynomial Zonotopes. arXiv preprint arXiv:2207.02715.



