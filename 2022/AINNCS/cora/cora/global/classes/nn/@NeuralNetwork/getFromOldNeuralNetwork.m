function obj = getFromOldNeuralNetwork(nn_old)
% getFromOldNeuralNetwork - transforms the old neuralNetwork to the new
% layer-based model.
%
% Syntax:
%    res = NeuralNetwork.getFromOldNeuralNetwork(nn_old)
%
% Inputs:
%    nn_old - network of type neuralNetwork
%
% Outputs:
%    res - generated network
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: NeuralNetwork (layer-based), neuralNetwork (old)

% Author:       Tobias Ladner
% Written:      28-March-2022
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

layers = cell(length(nn_old.W), 1);
for i = 1:length(nn_old.W)
    % add layers
    layers{2*i-1} = NNLinearLayer(nn_old.W{i}, nn_old.b{i});

    if nn_old.actFun{i} == "sigmoid"
        activation_layer = NNSigmoidLayer();
    elseif nn_old.actFun{i} == "tanh"
        activation_layer = NNTanhLayer();
    elseif nn_old.actFun{i} == "ReLU"
        activation_layer = NNReLULayer();
    elseif nn_old.actFun{i} == "identity"
        activation_layer = NNIdentityLayer();
    else
        error("Unknown activation function '%s'", nn_old.actFun{i})
    end
    layers{2*i} = activation_layer;
end

obj = NeuralNetwork(layers);
end