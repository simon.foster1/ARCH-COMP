function obj = readONNXNetwork(file_path, verbose, inputDataFormats, outputDataFormats)
% readONNXNetwork - reads and converts a network saved in onnx format
%
% Syntax:
%    res = NeuralNetwork.readONNXNetwork(file_path)
%
% Inputs:
%    file_path: path to file
%    verbose: bool if information should be displayed
%    inputDataFormats: dimensions of input e.g. 'BC' or 'BSSC'
%    outputDataFormats: see inputDataFormats
%
% Outputs:
%    obj - generated object
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: neuralnetwork2cora

% Author:       Tobias Ladner
% Written:      30-March-2022
% Last update:  07-June-2022 (specify in- & outputDataFormats)
% Last revision:---

%------------- BEGIN CODE --------------

% validate parameters
if nargin < 2
    verbose = false;
end
if nargin < 3
    inputDataFormats = 'BC';
end
if nargin < 4
    outputDataFormats = 'BC';
end

try
    if verbose
        disp("Reading Network: Try #1 ...")
    end

    dltoolbox_net = importONNXNetwork(file_path, 'InputDataFormats', inputDataFormats, 'OutputDataFormats', outputDataFormats);
    obj = NeuralNetwork.convertDLToolboxNetwork(dltoolbox_net.Layers, verbose);
catch ex
    try
        if verbose
            disp("Reading Network: Try #2 ...")
        end

        dltoolbox_net = importONNXNetwork(input, 'OutputLayerType', 'regression');
        obj = NeuralNetwork.convertDLToolboxNetwork(dltoolbox_net.Layers, verbose);
    catch
        try
            if verbose
                disp("Reading Network: Try #3 ...")
            end

            dltoolbox_net = importONNXNetwork(input, 'OutputLayerType', ...
                'regression', 'TargetNetwork', 'dlnetwork');
            % TODO: remove dependency on old neuralNetwork
            nn_old = constructFromParams(dltoolbox_net.Learnables.Value);
            obj = NeuralNetwork.getFromOldNeuralNetwork(nn_old);
        catch
            try
                if verbose
                    disp("Reading Network: Try #4 ...")
                end

                L = importONNXLayers(input, 'OutputLayerType', ...
                    'regression', 'ImportWeights', true);
                obj = NeuralNetwork.convertDLToolboxNetwork(L, verbose);
            catch
                error(ex.message);
            end
        end
    end
end

end

%------------- END CODE --------------


function NN = constructFromParams(params)
% construct a neural network from the learnable parameters

% TODO: remove dependency on old neuralNetwork

% extract weights and biases from the learnable parameters
W = {};
b = {};

for i = 1:length(params)
    if size(params{i}, 2) == 1
        b{end+1, 1} = double(extractdata(params{i}));
    else
        W{end+1, 1} = double(extractdata(params{i}))';
    end
end

% try to construct a neuralNetwork object
actFun = [repmat({'ReLU'}, [length(W) - 1, 1]); {'identity'}];

NN = neuralNetwork(W, b, actFun);

warning(['Could not determine activation functions,', ...
    ' so ReLUs are used as a default.']);
end