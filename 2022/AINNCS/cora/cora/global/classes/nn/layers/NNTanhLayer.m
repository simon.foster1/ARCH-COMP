classdef NNTanhLayer < NNSShapeLayer
    % NNTanhLayer - class for tanh layers
    %
    % Syntax:
    %    obj = NNTanhLayer(name)
    %
    % Inputs:
    %    name - name of the layer, defaults to type
    %
    % Outputs:
    %    obj - generated object
    %
    % Other m-files required: none
    % Subfunctions: none
    % MAT-files required: none
    %
    % See also: NeuralNetwork
    %
    % Author:       Tobias Ladner
    % Written:      28-March-2022
    % Last update:  ---
    % Last revision:---

    %------------- BEGIN CODE --------------

    properties (Constant)
        type = "TanhLayer"
    end

    methods
        % constructor
        function obj = NNTanhLayer(name)
            if nargin < 1
                name = NNTanhLayer.type;
            end
            % call super class constructor
            obj@NNSShapeLayer(name)
        end

        function [r, obj] = evaluateNumeric(obj, input)
            r = tanh(input);
        end

    end
end
