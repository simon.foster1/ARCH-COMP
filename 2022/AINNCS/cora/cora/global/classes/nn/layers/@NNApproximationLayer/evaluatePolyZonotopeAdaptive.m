function [c, G, Grest, expMat, id, id_, ind, ind_] = evaluatePolyZonotopeAdaptive(obj, c, G, Grest, expMat, id, id_, ind, ind_, evParams)
% evaluates the approximation layer on a polyZonotope using polynomials of
% adaptive order
%
% Syntax:
%    [c, G, Grest, expMat, id, id_, ind, ind_] = evaluatePolyZonotopeAdaptive(obj, c, G, Grest, expMat, id, id_, ind, ind_, evParams)
%
% Inputs:
%    c, G, Grest, expMat, id, id_, ind, ind_ - parameters of polyZonotope
%    evParams - parameter for NN evaluation
%
% Outputs:
%    updated [c, G, Grest, expMat, id, id_, ind, ind_]
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: NNActivationLayer/evaluatePolyZonotopeAdaptive
%
% Author:        Tobias Ladner
% Written:       06-April-2022
% Last update:   ---
% Last revision: ---
%
%------------- BEGIN CODE --------------

num_neurons_in = size(G, 1);
% for image inputs the input dimension is very large, so we
% perform an order reduction here to reduce comp. time
if ~isempty(evParams.num_generators) && evParams.i == 2 && ...
        size(G, 2) + size(Grest, 2) > evParams.num_generators
    [c, G, Grest, expMat, id] = NNHelper.initialOrderReduction(c, ...
        G, Grest, expMat, id, id_, evParams.num_generators);
    ind = find(prod(ones(size(expMat))- ...
        mod(expMat, 2), 1) == 1);
    ind_ = setdiff(1:size(expMat, 2), ind);
end

% initialization
order = obj.order;

% calculate exponential matrix
Es = cell(1, order);
Es_ext = cell(1, order);
Es{1} = expMat;
Es_ext{1} = expMat;

for i = 2:order
    % Note that e.g., G2 * G3 = G5 -> E2 + E3 = E5
    i1 = floor(i/2);
    i2 = ceil(i/2);

    Ei1_ext = Es_ext{i1};
    Ei2_ext = Es_ext{i2};
    Ei = NNHelper.calcSquaredE(Ei1_ext, Ei2_ext, i1 == i2);
    Ei_ext = [Ei1_ext, Ei2_ext, Ei];

    Es{i} = Ei;
    Es_ext{i} = Ei_ext;
end

% calculate coefficients

% calculate data points for regression
approx = evParams.bound_approx;
if isa(approx, 'logical')
    [l, u] = NNHelper.compBoundsPolyZono(c, G, Grest, expMat, ind, ind_, approx);
    S = interval(l, u);

elseif strcmp(approx, "sample")
    S = polyZonotope(c, G, Grest, expMat);

else
    error("Unknown Bound Estimation: %s", approx);
end

% TODO better sampling?
x = [S.randPoint(5000), S.randPoint(500, 'extreme')];
% include dim? Curse of dimensionality!
% dim = size(G, 1);
% x = [S.randPoint(5000 + max(10^dim, 10^5)), S.randPoint(500 + max(10^dim, 10^5), 'extreme')];
y = obj.evaluateNumeric(x);

% regression
X = cell2mat(arrayfun(@(x) x.^(0:order), x', 'UniformOutput', false));
Y = y';
C = pinv(X) * Y;

num_neurons_out = size(y, 1);

Ls = cell(num_neurons_out, 1);

plot_approx_info = evParams.plot_multi_layer_approx_info;
if plot_approx_info
    figure;
    hold on;
end

for m = 1:num_neurons_out
    C_m = C(:, m);
    C_m = reshape(C_m, order+1, num_neurons_in)';
    C_m = fliplr(C_m);

    y_poly = zeros(1, size(x, 2));
    for n = 1:num_neurons_in
        y_poly = y_poly + polyval(C_m(n, :), x(n, :));
    end

    tol = 0.00001;
    int = interval(min(y(m, :)-y_poly)-tol, max(y(m, :)-y_poly)+tol);
    Ls{m} = int;

    if plot_approx_info
        for n = 1:num_neurons_in
            subplot(num_neurons_in, num_neurons_out, (n - 1)*num_neurons_out+m)
            hold on;
            legend("Location", "best")
            xlabel(sprintf("Input Neuron: %d", n))
            ylabel(sprintf("Output Neuron: %d", m))

            d = rad(Ls{m});
            ce = center(Ls{m});

            scatter(x(n, :), y(m, :), 'k', 'Marker', '.', 'DisplayName', 'True')
            scatter(x(n, :), ce+y_poly, 'g', 'Marker', '.', 'DisplayName', 'Approx')
            scatter(x(n, :), ce+y_poly+d, 'b', 'Marker', '.', 'DisplayName', 'Upper Approx')
            scatter(x(n, :), ce+y_poly-d, 'c', 'Marker', '.', 'DisplayName', 'Lower Approx')

            low_enough = y(m, :) >= ce + y_poly - d;
            if ~all(low_enough)
                scatter(x(n, ~low_enough), y(m, ~low_enough), 'r', 'Marker', '.', 'DisplayName', 'Too High!')
            end

            high_enough = y(m, :) <= ce + y_poly + d;
            if ~all(high_enough)
                scatter(x(n, ~high_enough), y(m, ~high_enough), 'r', 'Marker', '.', 'DisplayName', 'Too Low!')
            end

            %             disp([n, m, all(low_enough), all(high_enough)])
        end
    end

end

% ----- NEW ----

% evaluate the polynomial approximation on the polynomial zonotope

% store information to directly compact generators
[G_start, G_end, G_ext_start, G_ext_end] = NNHelper.getOrderIndicesG(G, order);
[Grest_start, Grest_end, Grest_ext_start, Grest_ext_end] = NNHelper.getOrderIndicesGrest(Grest, G, order);

% init
c_out = zeros(num_neurons_in, order);
G_ext_out = zeros(num_neurons_in, G_ext_end(end));
Grest_ext_out = zeros(num_neurons_in, Grest_ext_end(end));

c_out(:, 1) = c;
G_ext_out(:, G_start(1):G_end(1)) = G;
Grest_ext_out(:, Grest_start(1):Grest_end(1)) = Grest;

c = zeros(num_neurons_out, 1);
G = zeros(num_neurons_out, G_end(end));
Grest = zeros(num_neurons_out, Grest_end(end));

% loop over all input neurons and create poly. terms
for n = 1:num_neurons_in
    for i = 2:order
        % Note that e.g., G2 * G3 = G5
        i1 = floor(i/2);
        i2 = ceil(i/2);

        % previous values
        ci1 = c_out(n, i1);
        Gi1 = G_ext_out(n, G_ext_start(i1):G_ext_end(i1));
        Gresti1 = Grest_ext_out(n, Grest_ext_start(i1):Grest_ext_end(i1));

        ci2 = c_out(n, i2);
        Gi2 = G_ext_out(n, G_ext_start(i2):G_ext_end(i2));
        Gresti2 = Grest_ext_out(n, Grest_ext_start(i2):Grest_ext_end(i2));

        % calculate i
        [ci, Gi_ext, Gresti_ext] = NNHelper.calcSquared(ci1, Gi1, Gresti1, ci2, Gi2, Gresti2, i1, i2, Es);

        % store results
        c_out(n, i) = ci;
        G_ext_out(n, G_ext_start(i):G_ext_end(i)) = Gi_ext;
        Grest_ext_out(n, Grest_ext_start(i):Grest_ext_end(i)) = Gresti_ext;
    end
end

d = zeros(num_neurons_out, 1);

% loop over all output neurons
for m = 1:num_neurons_out
    C_m = C(:, m);
    C_m = reshape(C_m, order+1, num_neurons_in)';

    L = Ls{m};
    d(m) = rad(L);

    % create copies to manipulate for each m
    c_out_m = c_out;
    G_ext_out_m = G_ext_out;
    Grest_ext_out_m = Grest_ext_out;

    % init
    G_out_m = zeros(1, G_end(end));
    Grest_out_m = zeros(1, Grest_end(end));

    % update weights with coefficients
    for i = 1:order
        coeff_i = C_m(:, 1+i);
        c_out_m(:, i) = c_out_m(:, i) .* coeff_i;
        G_ext_out_m(:, G_ext_start(i):G_ext_end(i)) = G_ext_out_m(:, G_ext_start(i):G_ext_end(i)) .* coeff_i;
        Grest_ext_out_m(:, Grest_ext_start(i):Grest_ext_end(i)) = Grest_ext_out_m(:, Grest_ext_start(i):Grest_ext_end(i)) .* coeff_i;
    end

    % sum column-wise to make next operations more efficient
    c_out_m = sum(c_out_m, 1);
    G_ext_out_m = sum(G_ext_out_m, 1);
    Grest_ext_out_m = sum(Grest_ext_out_m, 1);

    % update generators with same exponent from back to front:
    % i1 = floor(i/2); i2 = ceil(i/2)
    % G{i} = [Gi1, Gi2, Gi]
    % → G{i1} += Gi1
    % → G{i2} += Gi2
    % and add Gi to result
    % analogous for Grest{i} = [Gresti1, Gresti2, Gresti]

    for i = order:-1:2
        % Note that e.g., G2 * G3 = G5
        i1 = floor(i/2);
        i2 = ceil(i/2);

        % get generator lenghts
        i_len = G_end(i) - G_start(i) + 1;
        i1_len = G_ext_end(i1) - G_ext_start(i1) + 1;
        i2_len = G_ext_end(i2) - G_ext_start(i2) + 1;

        % extract lower order generators
        Gi1 = G_ext_out_m(1:end, G_ext_start(i)-1+(1:i1_len));
        Gi2 = G_ext_out_m(1:end, G_ext_start(i)+i1_len-1+(1:i2_len));
        Gi = G_ext_out_m(1:end, G_ext_start(i)+i1_len+i2_len-1+(1:i_len));

        % update generators
        G_ext_out_m(1:end, G_ext_start(i1):G_ext_end(i1)) = G_ext_out_m(1:end, G_ext_start(i1):G_ext_end(i1)) + Gi1;
        G_ext_out_m(1:end, G_ext_start(i2):G_ext_end(i2)) = G_ext_out_m(1:end, G_ext_start(i2):G_ext_end(i2)) + Gi2;
        G_out_m(1:end, G_start(i):G_end(i)) = Gi;

        % analogous for Grest if present
        if Grest_ext_end(i) > 0
            % get generator lenghts
            i_len = Grest_end(i) - Grest_start(i) + 1;
            i1_len = Grest_ext_end(i1) - Grest_ext_start(i1) + 1;
            i2_len = Grest_ext_end(i2) - Grest_ext_start(i2) + 1;

            % extract lower order generators
            Gresti1 = Grest_ext_out_m(1:end, Grest_ext_start(i)-1+(1:i1_len));
            Gresti2 = Grest_ext_out_m(1:end, Grest_ext_start(i)+i1_len-1+(1:i2_len));
            Gresti = Grest_ext_out_m(1:end, Grest_ext_start(i)+i1_len+i2_len-1+(1:i_len));

            % update lower order generators
            Grest_ext_out_m(1:end, Grest_ext_start(i1):Grest_ext_end(i1)) = Grest_ext_out_m(1:end, Grest_ext_start(i1):Grest_ext_end(i1)) + Gresti1;
            Grest_ext_out_m(1:end, Grest_ext_start(i2):Grest_ext_end(i2)) = Grest_ext_out_m(1:end, Grest_ext_start(i2):Grest_ext_end(i2)) + Gresti2;
            Grest_out_m(1:end, Grest_start(i):Grest_end(i)) = Gresti;
        end
    end

    % extract linear generators
    G_out_m(1:end, G_start(1):G_end(1)) = G_ext_out_m(1:end, G_ext_start(1):G_ext_end(1));
    Grest_out_m(1:end, Grest_start(1):Grest_end(1)) = Grest_ext_out_m(1:end, Grest_ext_start(1):Grest_ext_end(1));

    % add to output
    c(m, :) = sum(c_out_m) + sum(C_m(:, 1)); % add constant terms
    G(m, :) = G_out_m;
    Grest(m, :) = Grest_out_m;
end

% ----- NEW ----

expMat = cell2mat(Es);
[expMat, G] = removeRedundantExponents(expMat, G);

% order reduction
[c, G, Grest, expMat, id, d] = NNHelper.reducePolyZono(c, G, Grest, ...
    expMat, id, d, evParams.num_generators);

% save error bound for refinement
obj.refine_metric = d;

temp = diag(d);
temp = temp(:, d > 0);
if evParams.add_approx_error_to_Grest
    Grest = [Grest, temp];
else
    G = [G, temp];
    expMat = blkdiag(expMat, eye(size(temp, 2)));
    id = [id; 1 + (1:size(temp, 2))' * id_];
    id_ = max(id);
end

% update indices of all-even exponents (for zonotope encl.)
ind = find(prod(ones(size(expMat))-mod(expMat, 2), 1) == 1);
ind_ = setdiff(1:size(expMat, 2), ind);

end