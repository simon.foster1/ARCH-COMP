function res = in(hyp,S)
% in - determines if a constrained hyperplane contains a set or a point
%
% Syntax:  
%    res = in(hyp,S)
%
% Inputs:
%    hyp - conHyperplane object
%    S - contSet object or single point
%
% Example: 
%    hyp = conHyperplane(halfspace([1;1],0),[1 0;-1 0],[2;2]);
%    point = [0;0];
%    in(hyp,point)
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: conHyperplane/isIntersecting

% Author:       Victor Gassmann
% Written:      19-July-2021
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

% containment check
res = in(mptPolytope(hyp),S);

%------------- END OF CODE --------------