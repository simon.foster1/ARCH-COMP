function res = isequal(Z1,Z2,varargin)
% isequal - checks if two zonotopes are equal (note: no deletion of aligned
%    generators since this is quite costly)
%
% Syntax:  
%    res = isequal(Z1,Z2,tol)
%
% Inputs:
%    Z1 - zonotope object
%    Z2 - zonotope object
%    tol - (optional) tolerance
%
% Outputs:
%    res - true/false
%
% Example: 
%    Z1 = zonotope([zeros(3,1),rand(3,5)]);
%    Z2 = zonotope([zeros(3,1),rand(3,5)]);
%    isequal(Z1,Z2);
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:       Mark Wetzlinger
% Written:      16-Sep-2019
% Last update:  09-June-2020 (include re-ordering of generators)
% Last revision:---

%------------- BEGIN CODE --------------

% parse input arguments
tol = setDefaultValues({{eps}},varargin{:});

% check input arguments
inputArgsCheck({{Z1,'att',{'zonotope'},{''}};
                {Z2,'att',{'zonotope'},{''}};
                {tol,'att',{'numeric'},{'nonnan','scalar','nonnegative'}}});

% init result
res = false;

% compare dimensions (quick check)
if dim(Z1) ~= dim(Z2)
    return
end

% compare centers (quick check)
if ~all(withinTol(center(Z1),center(Z2),tol))
    return
end

% 1D case
if dim(Z1) == 1 % equal to dim(Z2), see above
    % reduce generator matrices to one generator
    g1red = sum(abs(generators(Z1)));
    g2red = sum(abs(generators(Z2)));
    min_g = min(g1red,g2red);
    tol = 1e-12;
    if (withinTol(min_g,0) && withinTol(g1red,g2red,tol))
        res = true;
    end
    return
end

G1 = generators(deleteZeros(Z1));
G2 = generators(deleteZeros(Z2));
% compare generator matrices
nrOfGens = size(G1,2);
if nrOfGens ~= size(G2,2)
    return
elseif all(all(withinTol(G1,G2,tol)))
    % both already ordered in the same way
    res = true;
    return
else
    % sort to obtain correct correspondances
    [~,idx] = sortrows(G1',[1,2]);
    G1 = G1(:,idx);
    [~,idx] = sortrows(G2',[1,2]);
    G2 = G2(:,idx);
    if all(all(withinTol(G1,G2,tol)))
        res = true;
        return
    end
end

%------------- END OF CODE --------------