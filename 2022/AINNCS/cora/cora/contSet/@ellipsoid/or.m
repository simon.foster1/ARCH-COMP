function E = or(E,S,varargin)
% or - overloads '|' operator to compute an inner-/outer-approximation of
%    the union of an ellipsoid and another set representation
%
% Syntax:  
%    E = or(E,S)
%    E = or(E,S,mode)
%
% Inputs:
%    E - ellipsoid object
%    S - contSet object (or array)
%    mode - (optional) 'i' (inner-approximation) / 'o' (outer-approximation)
%
% Outputs:
%    E - ellipsoid object
%
% Example: 
%    E1 = ellipsoid.generateRandom('Dimension',2);
%    E2 = ellipsoid.generateRandom('Dimension',2);
%    E = E1 | E2;
%
% References:
%    Convex Optimization; Boyd
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:       Victor Gassmann
% Written:      09-March-2021 
% Last update:  15-March-2021
%               04-July-2022 (VG: class array; fixed small bug)
% Last revision:---

%------------- BEGIN CODE --------------

%% parsing & checking
% make sure first argument is class argument 
[E,S] = findClassArg(E,S,'ellipsoid');
[mode] = setDefaultValues({{'o'}},varargin{:});

% check input arguments
inputArgsCheck({{E,'att',{'ellipsoid'},{'scalar'}};
                {S,'att',{'contSet','numeric'},{''}};
                {mode,'str',{'o','i'}}});

% check if equal dims
equalDimCheck(E,S);


if all(isempty(S))
    return;
end

%% different unions
if isa(S,'double')
    V = S;
    if strcmp(mode,'o')
        E_p = ellipsoid.enclosePoints(V,'min-vol');
        E = orEllipsoidOA([E,E_p]);
        return;
    else
        P = mptPolytope.enclosePoints(V);
        E = or(E,P,'i');
        return;
    end
end

if isa(S,'ellipsoid')
    if strcmp(mode,'o')
        E = orEllipsoidOA([E;S(:)]);
        return;
    else
        throw(CORAerror('CORA:noops',E,S,'i'));
    end
end

if isa(S,'mptPolytope')
    if strcmp(mode,'o')
        E_S = ellipsoid.array(1,numel(S));
        for i=1:numel(S)
            E_S(i) = ellipsoid(S,'o');
        end
        E = orEllipsoid([E,E_S]);
    else
        % not implemented
        %E = orEllipsoidIA([E,ellipsoid(S,'i')]);
        throw(CORAerror('CORA:noops',E,S));
    end
end

% throw error for all other combinations
throw(CORAerror('CORA:noops',E,S));

%------------- END OF CODE --------------