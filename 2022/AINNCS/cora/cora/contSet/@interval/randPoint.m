function p = randPoint(I,varargin)
% randPoint - computes random point in interval
%
% Syntax:  
%    p = randPoint(I)
%    p = randPoint(I,N)
%    p = randPoint(I,type)
%    p = randPoint(I,N,type)
%    p = randPoint(I,'all','extreme')
%    p = randPoint(I,N,'gaussian',pr)
%    p = randPoint(I,'gaussian',pr)
%
% Inputs:
%    I - interval object
%    N - number of random points
%    type - type of the random point ('standard', 'extreme', or 'gaussian')
%    pr - probability that a value is within the set (only type = 'gaussian')
%
% Outputs:
%    p - random point in interval
%
% Example: 
%    I = interval([-2;1],[3;2]);
%    p = randPoint(I,20);
%
%    figure; hold on;
%    plot(I);
%    plot(p(1,:),p(2,:),'.k','MarkerSize',10);
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: zonotope/randPoint

% Author:       Mark Wetzlinger
% Written:      17-Sep-2019
% Last update:  25-June-2021 (MP, add type gaussian)
% Last revision:---

%------------- BEGIN CODE --------------

    % parse input arguments
    N = 1;
    type = 'standard';
    types = {'standard','extreme','gaussian'};
    
    if nargin > 1 && ~isempty(varargin{1})
        if (isnumeric(varargin{1}) && isscalar(varargin{1})) || ...
                (ischar(varargin{1}) && strcmp(varargin{1},'all'))
            % second input argument is number of points
            N = varargin{1};
            
            if nargin > 2
                if ischar(varargin{2}) && any(strcmp(varargin{2},types))
                    type = varargin{2};
                    if nargin > 3  && strcmp(type,'gaussian')
                        pr = varargin{3};
                    end
                end
            end
            
        elseif ischar(varargin{1}) && any(strcmp(varargin{1},types))
            % second input argument is type (sampling method)
            type = varargin{1};
            if nargin > 2 && strcmp(type,'gaussian')
                pr = varargin{2};
            end
            
        else
            throw(CORAerror('CORA:wrongValue','second or third',...
                "N - number of random points \n type - 'standard','extreme' or 'gaussian'"));
        end
    end
    
    
    % generate different types of extreme points
    if strcmp(type,'gaussian')
        if nargin == 3
            p = randPoint@contSet(I,type,pr);
        else
            p = randPoint@contSet(I,N,type,pr);
        end
    else
        % get object properties
        c = center(I); r = rad(I); n = dim(I);
        
        if isempty(I)
            % empty set
            throw(CORAerror('CORA:emptySet'));
        end
        
        if strcmp(type,'standard')
            
            if size(r,2) > 1
                if size(r,1) > 1
                    % both dimensions larger than 1 -> interval matrix
                    throw(CORAerror('CORA:wrongInputInConstructor',...
                        'interval/randPoint not defined for interval matrices!'));
                else
                    p = c + (-1 + 2 * rand(N,length(r))) .* r;
                end
            else
                p = c + (-1 + 2 * rand(length(r),N)) .* r;
            end
            
        elseif strcmp(type,'extreme')
            
            % consider degenerate case
            ind = find(r > 0);
            if length(ind) < n
                I = project(I,ind);
                temp = randPoint(I,N,type);
                p = c * ones(1,N);
                p(ind,:) = temp;
                return;
            end
            
            % return all extreme point
            if ischar(N) && strcmp(N,'all')
                
                p = vertices(I);
                
                % generate random vertices
            elseif 10*N < 2^n
                
                p = zeros(n,N); cnt = 1;
                while cnt <= N
                    temp = sign(-1 + 2*rand(n,1));
                    if ~ismember(temp',p','rows')
                        p(:,cnt) = temp; cnt = cnt + 1;
                    end
                end
                p = c + p.*r;
                
                % select random vertices
            elseif N <= 2^n
                
                V = vertices(I);
                ind = randperm(size(V,2));
                V = V(:,ind);
                p = V(:,1:N);
                
                % compute vertices and additional points on the boundary
            else
                
                V = vertices(I);
                p = [V, zeros(n,N-size(V,2))];
                
                for i = size(V,2)+1:N
                    temp = sign(-1 + 2*rand(n,1));
                    ind = randi([1,n]);
                    temp(ind) = -1 + 2*rand();
                    p(:,i) = c + temp .* r;
                end
            end
        else
            throw(CORAerror('CORA:wrongValue','third',...
                "'standard', 'extreme' or 'gaussian'"));
        end
    end
end

%------------- END OF CODE --------------