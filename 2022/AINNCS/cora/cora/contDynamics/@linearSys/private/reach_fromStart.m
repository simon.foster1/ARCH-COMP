function [Rout,Rout_tp,res] = reach_fromStart(obj,options)
% reach - computes the reachable set for linear systems using the
%    propagation of the homogeneous solution from the start
%
% Syntax:  
%    [Rout,Rout_tp,res] = reach_fromStart(obj,options)
%
% Inputs:
%    obj - continuous system object
%    options - options for the computation of reachable sets
%
% Outputs:
%    Rout - array of time-interval reachable / output sets
%    Rout_tp - array of time-point reachable / output sets
%    res - boolean (only if specification given)
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:        Mark Wetzlinger
% Written:       26-June-2019
% Last update:   14-Aug-2019
%                16-February-2021 (MW, correct implementation of uTransVec)
% Last revision: ---

%------------- BEGIN CODE --------------

% obtain factors for initial state and input solution
for i=1:(options.taylorTerms+1)
    % compute initial state factor
    options.factor(i) = options.timeStep^(i)/factorial(i);    
end

% if a trajectory should be tracked
if isfield(options,'uTransVec')
    options.uTrans = options.uTransVec(:,1);
end

% log information
verboseLog(1,options.tStart,options);

% init step 
[Rnext, options] = initReach_Euclidean(obj, options.R0, options);
% init for loop
Rtrans = options.Rtrans;
Rinhom = options.Rpar;
Raux = options.Raux;
if ~isfield(options,'uTransVec')
    Rhom_0 = options.Rhom;
    Rhom_tp_0 = options.Rhom_tp;
    inputCorr = 0;
else
    Rhom_tp = options.Rhom_tp;
end
eADelta = obj.taylor.eAt;
P = eye(obj.dim);
Q = obj.taylor.eAt;


% time period, number of steps, step counter
tVec = options.tStart:options.timeStep:options.tFinal;
steps = length(tVec) - 1;
options.i = 1;

% initialize output variables for reachable sets and output sets
Rout = cell(steps,1);
Rout_tp = cell(steps+1,1);

% compute output set of first step
[Rout_tp{1},Rout{1}] = outputSet(obj,options,options.R0,Rnext.ti);
Rstart = Rnext.tp;

% safety property check
if isfield(options,'specification')
    if ~check(options.specification,Rout{1},interval(tVec(1),tVec(2)))
        % violation
        Rout = Rout(1);
        Rout_tp = Rout_tp(1);
        res = false;
        return
    end
end


for i = 2:steps
    
    options.i = i;
    
    % post --------------
    
    % if a trajectory should be tracked
    if isfield(options,'uTransVec')
        options.uTrans = options.uTransVec(:,i);
        options.Rhom_tp = Rhom_tp;
        % recompute Rtrans/inputCorr -> also propagate Rhom/Rhom_tp
        [Rhom,Rhom_tp,Rtrans,inputCorr] = inputInducedUpdates(obj,options);
        % reduce homogeneous part
        Rhom_tp = reduce(Rhom_tp,options.reductionTechnique,options.zonotopeOrder);
        % propagate inhomogeneous part
        Rinhom = eADelta * Rinhom + Raux;
    else
        % propagate homogeneous part (from start -> take Rhom_0)
        Rhom    = Q * Rhom_0;
        Rhom_tp = Q * Rhom_tp_0;
        % no uTransVec -> Rtrans is constant
        Rinhom = Rinhom + Q * Raux + P * Rtrans;
    end
    
    % reduce (only inhomogeneous solution)
    Rinhom = reduce(Rinhom,options.reductionTechnique,options.zonotopeOrder);
    
    % R([t_k, t_k + Delta t_k]) = H([t_k, t_k + Delta t_k]) + P([0, t_k])
    Rnext.ti = Rhom + Rinhom + inputCorr;
    Rnext.tp = Rhom_tp + Rinhom;
    
    % propagate matrix exponentials
    P = Q;
    Q = Q * eADelta;
    
    % --------------

    % compute output set
    [Rout_tp{i},Rout{i}] = outputSet(obj,options,Rstart,Rnext.ti);
    
    % save reachable set
    Rstart = Rnext.tp;
    
    % safety property check
    if isfield(options,'specification')
        if ~check(options.specification,Rout{i},interval(tVec(i),tVec(i+1)))
            % violation
            Rout = Rout(1:i);
            Rout_tp = Rout_tp(1:i);
            res = false;
            return
        end
    end
    
    % log information
    verboseLog(i,tVec(i),options);
    
end

% compute output set of last set
Rout_tp{end} = outputSet(obj,options,Rstart,Rnext.ti);

% specification fulfilled
res = true;

end

%------------- END OF CODE --------------