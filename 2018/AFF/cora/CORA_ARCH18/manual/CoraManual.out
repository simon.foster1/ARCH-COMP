\BOOKMARK [1][-]{section.1}{What's new compared to CORA 2016?}{}% 1
\BOOKMARK [1][-]{section.2}{Philosophy and Architecture}{}% 2
\BOOKMARK [1][-]{section.3}{Installation}{}% 3
\BOOKMARK [1][-]{section.4}{Connections to and from SpaceEx}{}% 4
\BOOKMARK [2][-]{subsection.4.1}{Importing SpaceEx Models}{section.4}% 5
\BOOKMARK [2][-]{subsection.4.2}{CORA/SX}{section.4}% 6
\BOOKMARK [1][-]{section.5}{Architecture}{}% 7
\BOOKMARK [1][-]{section.6}{Set Representations and Operations}{}% 8
\BOOKMARK [2][-]{subsection.6.1}{Zonotopes}{section.6}% 9
\BOOKMARK [3][-]{subsubsection.6.1.1}{Method mtimes}{subsection.6.1}% 10
\BOOKMARK [3][-]{subsubsection.6.1.2}{Method plus}{subsection.6.1}% 11
\BOOKMARK [3][-]{subsubsection.6.1.3}{Method reduce}{subsection.6.1}% 12
\BOOKMARK [3][-]{subsubsection.6.1.4}{Method split}{subsection.6.1}% 13
\BOOKMARK [3][-]{subsubsection.6.1.5}{Zonotope Example}{subsection.6.1}% 14
\BOOKMARK [2][-]{subsection.6.2}{Zonotope Bundles}{section.6}% 15
\BOOKMARK [3][-]{subsubsection.6.2.1}{Zonotope Bundle Example}{subsection.6.2}% 16
\BOOKMARK [2][-]{subsection.6.3}{Polynomial Zonotopes}{section.6}% 17
\BOOKMARK [3][-]{subsubsection.6.3.1}{Method reduce}{subsection.6.3}% 18
\BOOKMARK [3][-]{subsubsection.6.3.2}{Polynomial Zonotope Example}{subsection.6.3}% 19
\BOOKMARK [2][-]{subsection.6.4}{Probabilistic Zonotopes}{section.6}% 20
\BOOKMARK [3][-]{subsubsection.6.4.1}{Probabilistic Zonotope Example}{subsection.6.4}% 21
\BOOKMARK [2][-]{subsection.6.5}{Constrained Zonotopes}{section.6}% 22
\BOOKMARK [3][-]{subsubsection.6.5.1}{Method and}{subsection.6.5}% 23
\BOOKMARK [3][-]{subsubsection.6.5.2}{Method enclose}{subsection.6.5}% 24
\BOOKMARK [3][-]{subsubsection.6.5.3}{Method plus}{subsection.6.5}% 25
\BOOKMARK [3][-]{subsubsection.6.5.4}{Method reduce}{subsection.6.5}% 26
\BOOKMARK [3][-]{subsubsection.6.5.5}{Constrained Zonotope Example}{subsection.6.5}% 27
\BOOKMARK [2][-]{subsection.6.6}{MPT Polytopes}{section.6}% 28
\BOOKMARK [3][-]{subsubsection.6.6.1}{MPT Polytope Example}{subsection.6.6}% 29
\BOOKMARK [2][-]{subsection.6.7}{Intervals}{section.6}% 30
\BOOKMARK [3][-]{subsubsection.6.7.1}{Interval Example}{subsection.6.7}% 31
\BOOKMARK [2][-]{subsection.6.8}{Taylor Models}{section.6}% 32
\BOOKMARK [3][-]{subsubsection.6.8.1}{Initialization}{subsection.6.8}% 33
\BOOKMARK [3][-]{subsubsection.6.8.2}{Mathematical operations}{subsection.6.8}% 34
\BOOKMARK [3][-]{subsubsection.6.8.3}{Bound estimators}{subsection.6.8}% 35
\BOOKMARK [3][-]{subsubsection.6.8.4}{Interval substitution}{subsection.6.8}% 36
\BOOKMARK [3][-]{subsubsection.6.8.5}{Branch and bound}{subsection.6.8}% 37
\BOOKMARK [3][-]{subsubsection.6.8.6}{Small coefficients pruning}{subsection.6.8}% 38
\BOOKMARK [3][-]{subsubsection.6.8.7}{List of the functions realized in CORA}{subsection.6.8}% 39
\BOOKMARK [3][-]{subsubsection.6.8.8}{Taylor Model Example}{subsection.6.8}% 40
\BOOKMARK [2][-]{subsection.6.9}{Vertices}{section.6}% 41
\BOOKMARK [3][-]{subsubsection.6.9.1}{Vertices Example}{subsection.6.9}% 42
\BOOKMARK [2][-]{subsection.6.10}{Plotting of Sets}{section.6}% 43
\BOOKMARK [1][-]{section.7}{Matrix Set Representations and Operations}{}% 44
\BOOKMARK [2][-]{subsection.7.1}{Matrix Polytopes}{section.7}% 45
\BOOKMARK [3][-]{subsubsection.7.1.1}{Matrix Polytope Example}{subsection.7.1}% 46
\BOOKMARK [2][-]{subsection.7.2}{Matrix Zonotopes}{section.7}% 47
\BOOKMARK [3][-]{subsubsection.7.2.1}{Matrix Zonotope Example}{subsection.7.2}% 48
\BOOKMARK [2][-]{subsection.7.3}{Interval Matrices}{section.7}% 49
\BOOKMARK [3][-]{subsubsection.7.3.1}{Interval Matrix Example}{subsection.7.3}% 50
\BOOKMARK [1][-]{section.8}{Continuous Dynamics}{}% 51
\BOOKMARK [2][-]{subsection.8.1}{Linear Systems}{section.8}% 52
\BOOKMARK [3][-]{subsubsection.8.1.1}{Method initReach}{subsection.8.1}% 53
\BOOKMARK [2][-]{subsection.8.2}{Linear Systems with Uncertain Fixed Parameters}{section.8}% 54
\BOOKMARK [3][-]{subsubsection.8.2.1}{Method initReach}{subsection.8.2}% 55
\BOOKMARK [2][-]{subsection.8.3}{Linear Systems with Uncertain Varying Parameters}{section.8}% 56
\BOOKMARK [2][-]{subsection.8.4}{Linear Probabilistic Systems}{section.8}% 57
\BOOKMARK [3][-]{subsubsection.8.4.1}{Method initReach}{subsection.8.4}% 58
\BOOKMARK [2][-]{subsection.8.5}{Nonlinear Systems}{section.8}% 59
\BOOKMARK [3][-]{subsubsection.8.5.1}{Method initReach}{subsection.8.5}% 60
\BOOKMARK [2][-]{subsection.8.6}{Nonlinear Systems with Uncertain Fixed Parameters}{section.8}% 61
\BOOKMARK [2][-]{subsection.8.7}{Nonlinear Differential-Algebraic Systems}{section.8}% 62
\BOOKMARK [1][-]{section.9}{Hybrid Dynamics}{}% 63
\BOOKMARK [2][-]{subsection.9.1}{Simulation of Hybrid Automata}{section.9}% 64
\BOOKMARK [2][-]{subsection.9.2}{Hybrid Automaton}{section.9}% 65
\BOOKMARK [2][-]{subsection.9.3}{Location}{section.9}% 66
\BOOKMARK [2][-]{subsection.9.4}{Transition}{section.9}% 67
\BOOKMARK [1][-]{section.10}{State Space Partitioning}{}% 68
\BOOKMARK [2][-]{subsection.10.1}{Migrating the old partition Class into the new one}{section.10}% 69
\BOOKMARK [1][-]{section.11}{Options for Reachability Analysis}{}% 70
\BOOKMARK [1][-]{section.12}{Unit Tests}{}% 71
\BOOKMARK [1][-]{section.13}{Loading Simulink and SpaceEx Models}{}% 72
\BOOKMARK [2][-]{subsection.13.1}{Converting Simulink Models to SpaceEx Models}{section.13}% 73
\BOOKMARK [2][-]{subsection.13.2}{Loading SpaceEx Models}{section.13}% 74
\BOOKMARK [1][-]{section.14}{Examples}{}% 75
\BOOKMARK [2][-]{subsection.14.1}{Continuous Dynamics}{section.14}% 76
\BOOKMARK [3][-]{subsubsection.14.1.1}{Linear Dynamics}{subsection.14.1}% 77
\BOOKMARK [3][-]{subsubsection.14.1.2}{Linear Dynamics with Uncertain Parameters}{subsection.14.1}% 78
\BOOKMARK [3][-]{subsubsection.14.1.3}{Nonlinear Dynamics}{subsection.14.1}% 79
\BOOKMARK [3][-]{subsubsection.14.1.4}{Nonlinear Dynamics with Uncertain Parameters}{subsection.14.1}% 80
\BOOKMARK [3][-]{subsubsection.14.1.5}{Nonlinear Differential-Algebraic Systems}{subsection.14.1}% 81
\BOOKMARK [2][-]{subsection.14.2}{Hybrid Dynamics}{section.14}% 82
\BOOKMARK [3][-]{subsubsection.14.2.1}{Bouncing Ball Example}{subsection.14.2}% 83
\BOOKMARK [3][-]{subsubsection.14.2.2}{Powertrain Example}{subsection.14.2}% 84
\BOOKMARK [1][-]{section.15}{Conclusions}{}% 85
\BOOKMARK [1][-]{appendix.A}{Migrating the intervalhull Class into the interval Class}{}% 86
\BOOKMARK [1][-]{appendix.B}{Implementation of Loading SpaceEx Models}{}% 87
\BOOKMARK [2][-]{subsection.B.1}{Preface: The SpaceEx Format}{appendix.B}% 88
\BOOKMARK [2][-]{subsection.B.2}{Parsing The Component Definitions}{appendix.B}% 89
\BOOKMARK [3][-]{subsubsection.B.2.1}{Accessing XML Files}{subsection.B.2}% 90
\BOOKMARK [3][-]{subsubsection.B.2.2}{Storing The Component Definitions}{subsection.B.2}% 91
\BOOKMARK [2][-]{subsection.B.3}{"Instantiating" the Component Tree}{appendix.B}% 92
\BOOKMARK [2][-]{subsection.B.4}{Forming the Parallel Composition}{appendix.B}% 93
\BOOKMARK [3][-]{subsubsection.B.4.1}{Combining Flow Functions}{subsection.B.4}% 94
\BOOKMARK [3][-]{subsubsection.B.4.2}{Combining Invariants}{subsection.B.4}% 95
\BOOKMARK [3][-]{subsubsection.B.4.3}{Combining Transitions}{subsection.B.4}% 96
\BOOKMARK [2][-]{subsection.B.5}{Formalizing Symbolic Dynamics}{appendix.B}% 97
\BOOKMARK [3][-]{subsubsection.B.5.1}{Linear \046 Non-linear Flows}{subsection.B.5}% 98
\BOOKMARK [3][-]{subsubsection.B.5.2}{Polyhedric Guard \046 Invariant Sets}{subsection.B.5}% 99
\BOOKMARK [2][-]{subsection.B.6}{Conversion to MATLAB Function}{appendix.B}% 100
\BOOKMARK [2][-]{subsection.B.7}{Open Problems}{appendix.B}% 101
\BOOKMARK [1][-]{appendix.C}{Licensing}{}% 102
\BOOKMARK [1][-]{appendix.D}{Disclaimer}{}% 103
\BOOKMARK [1][-]{appendix.E}{Contributors}{}% 104
