The class \texttt{nonlinDASys} considers time-invariant, semi-explicit, index-1 DAEs without parametric uncertainties since they are not yet implemented. The extension to parametric uncertainties can be done using the methods applied in Sec. \ref{sec:nonlinearParamSystems}. Using the vectors of differential variables $x$, algebraic variables $y$, and inputs $u$, the semi-explicit DAE can generally be written as
\begin{equation}\label{eq:DAEsystem}
\begin{gathered}
 \dot{x} = f(x(t),y(t),u(t)) \\
 0 = g(x(t),y(t),u(t)), \\
 [x^T(0), y^T(0)]^T \in \mathcal{R}(0), \quad u(t)\in\mathcal{U}, 
\end{gathered}
\end{equation}
where $\mathcal{R}(0)$ over-approximates the set of consistent initial states and $\mathcal{U}$ is the set of possible inputs. The initial state is consistent when $g(x(0),y(0),u(0)) = 0$, while for DAEs with an index greater than $1$, further hidden algebraic constraints have to be considered \cite[Chapter 9.1]{Ascher1998}. For an implicit DAE, the index-1 property holds if and only if $\forall t: \, \det(\frac{\partial g(x(t),y(t),u(t))}{\partial y})\neq 0$, i.e. the Jacobian of the algebraic equations is non-singular \cite[p. 34]{Brenan1989}. Loosely speaking, the index specifies the distance to an ODE (which has index $0$) by the number of required time differentiations of the general form $0 = F(\dot{\tilde{x}}, \tilde{x}, u, t)$ along a solution $\tilde{x}(t)$, in order to express $\dot{\tilde{x}}$ as a continuous function of $\tilde{x}$ and $t$ \cite[Chapter 9.1]{Ascher1998}. 

To apply the methods presented in the previous section to compute reachable sets for DAEs, an abstraction of the original nonlinear DAEs to linear differential inclusions is performed for each consecutive time interval $\tau_k$. A different abstraction is used for each time interval to minimize the over-approximation error. Based on a linearization of the functions $f(x(t),y(t),u(t))$ and $g(x(t),y(t),u(t))$, one can abstract the dynamics of the original nonlinear DAE by a linear system plus additive uncertainty as detailed in \cite[Section IV]{Althoff2014a}. This linear system only contains dynamic state variables $x$ and uncertain inputs $u$. The algebraic state $y$ is obtained afterwards by the linearized constraint function $g(x(t),y(t),u(t))$ as described in \cite[Proposition 2]{Althoff2014a}.


Since the \texttt{nonlinDASys} class is written using the new structure for object oriented programming in MATLAB, it has the following public properties:
\begin{itemize}
 \item \texttt{dim} -- number of dimensions.
 \item \texttt{nrOfConstraints} -- number of constraints.
 \item \texttt{nrOfInputs} -- number of inputs.
 \item \texttt{dynFile} -- handle to the m-file containing the dynamic function $f(x(t),y(t),u(t))$.
 \item \texttt{conFile} -- handle to the m-file containing the constraint function $g(x(t),y(t),u(t))$.
 \item \texttt{jacobian} -- handle to the m-file containing the Jacobians of the dynamic function and the constraint function.
 \item \texttt{hessian} -- handle to the m-file containing the Hessians of the dynamic function and the constraint function.
 \item \texttt{thirdOrderTensor} -- handle to the m-file containing the third order tensors of the dynamic function and the constraint function.
 \item \texttt{linError} -- handle to the m-file containing the Lagrangian remainder.
 \item \texttt{other} -- other information.
\end{itemize}