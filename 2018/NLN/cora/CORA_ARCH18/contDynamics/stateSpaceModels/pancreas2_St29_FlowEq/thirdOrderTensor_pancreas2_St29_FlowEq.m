function Tf = thirdOrderTensor_pancreas2_St29_FlowEq(x,u)



 Tf{1,1} = interval(sparse(13,13),sparse(13,13));



 Tf{1,2} = interval(sparse(13,13),sparse(13,13));



 Tf{1,3} = interval(sparse(13,13),sparse(13,13));



 Tf{1,4} = interval(sparse(13,13),sparse(13,13));



 Tf{1,5} = interval(sparse(13,13),sparse(13,13));



 Tf{1,6} = interval(sparse(13,13),sparse(13,13));



 Tf{1,7} = interval(sparse(13,13),sparse(13,13));



 Tf{1,8} = interval(sparse(13,13),sparse(13,13));



 Tf{1,9} = interval(sparse(13,13),sparse(13,13));



 Tf{1,10} = interval(sparse(13,13),sparse(13,13));



 Tf{1,11} = interval(sparse(13,13),sparse(13,13));



 Tf{1,12} = interval(sparse(13,13),sparse(13,13));



 Tf{1,13} = interval(sparse(13,13),sparse(13,13));



 Tf{2,1} = interval(sparse(13,13),sparse(13,13));



 Tf{2,2} = interval(sparse(13,13),sparse(13,13));



 Tf{2,3} = interval(sparse(13,13),sparse(13,13));



 Tf{2,4} = interval(sparse(13,13),sparse(13,13));



 Tf{2,5} = interval(sparse(13,13),sparse(13,13));



 Tf{2,6} = interval(sparse(13,13),sparse(13,13));



 Tf{2,7} = interval(sparse(13,13),sparse(13,13));



 Tf{2,8} = interval(sparse(13,13),sparse(13,13));



 Tf{2,9} = interval(sparse(13,13),sparse(13,13));



 Tf{2,10} = interval(sparse(13,13),sparse(13,13));



 Tf{2,11} = interval(sparse(13,13),sparse(13,13));



 Tf{2,12} = interval(sparse(13,13),sparse(13,13));



 Tf{2,13} = interval(sparse(13,13),sparse(13,13));



 Tf{3,1} = interval(sparse(13,13),sparse(13,13));



 Tf{3,2} = interval(sparse(13,13),sparse(13,13));



 Tf{3,3} = interval(sparse(13,13),sparse(13,13));



 Tf{3,4} = interval(sparse(13,13),sparse(13,13));



 Tf{3,5} = interval(sparse(13,13),sparse(13,13));



 Tf{3,6} = interval(sparse(13,13),sparse(13,13));



 Tf{3,7} = interval(sparse(13,13),sparse(13,13));



 Tf{3,8} = interval(sparse(13,13),sparse(13,13));



 Tf{3,9} = interval(sparse(13,13),sparse(13,13));



 Tf{3,10} = interval(sparse(13,13),sparse(13,13));



 Tf{3,11} = interval(sparse(13,13),sparse(13,13));



 Tf{3,12} = interval(sparse(13,13),sparse(13,13));



 Tf{3,13} = interval(sparse(13,13),sparse(13,13));



 Tf{4,1} = interval(sparse(13,13),sparse(13,13));

Tf{4,1}(4,4) = 1170932905520417/1844674407370955161600 - (20015729765729043811741804506225*x(4))/10889035741470030830827987437816582766592;


 Tf{4,2} = interval(sparse(13,13),sparse(13,13));



 Tf{4,3} = interval(sparse(13,13),sparse(13,13));



 Tf{4,4} = interval(sparse(13,13),sparse(13,13));

Tf{4,4}(4,1) = 1170932905520417/1844674407370955161600 - (20015729765729043811741804506225*x(4))/10889035741470030830827987437816582766592;
Tf{4,4}(1,4) = 1170932905520417/1844674407370955161600 - (20015729765729043811741804506225*x(4))/10889035741470030830827987437816582766592;
Tf{4,4}(4,4) = - (20015729765729043811741804506225*x(1))/10889035741470030830827987437816582766592 - 64481584699558614303102720544341/340282366920938463463374607431768211456;


 Tf{4,5} = interval(sparse(13,13),sparse(13,13));



 Tf{4,6} = interval(sparse(13,13),sparse(13,13));



 Tf{4,7} = interval(sparse(13,13),sparse(13,13));



 Tf{4,8} = interval(sparse(13,13),sparse(13,13));



 Tf{4,9} = interval(sparse(13,13),sparse(13,13));



 Tf{4,10} = interval(sparse(13,13),sparse(13,13));



 Tf{4,11} = interval(sparse(13,13),sparse(13,13));



 Tf{4,12} = interval(sparse(13,13),sparse(13,13));



 Tf{4,13} = interval(sparse(13,13),sparse(13,13));



 Tf{5,1} = interval(sparse(13,13),sparse(13,13));



 Tf{5,2} = interval(sparse(13,13),sparse(13,13));



 Tf{5,3} = interval(sparse(13,13),sparse(13,13));



 Tf{5,4} = interval(sparse(13,13),sparse(13,13));



 Tf{5,5} = interval(sparse(13,13),sparse(13,13));



 Tf{5,6} = interval(sparse(13,13),sparse(13,13));



 Tf{5,7} = interval(sparse(13,13),sparse(13,13));



 Tf{5,8} = interval(sparse(13,13),sparse(13,13));



 Tf{5,9} = interval(sparse(13,13),sparse(13,13));



 Tf{5,10} = interval(sparse(13,13),sparse(13,13));



 Tf{5,11} = interval(sparse(13,13),sparse(13,13));

Tf{5,11}(12,11) = 6727672186155411/302231454903657293676544;
Tf{5,11}(11,12) = 6727672186155411/302231454903657293676544;


 Tf{5,12} = interval(sparse(13,13),sparse(13,13));

Tf{5,12}(11,11) = 6727672186155411/302231454903657293676544;


 Tf{5,13} = interval(sparse(13,13),sparse(13,13));



 Tf{6,1} = interval(sparse(13,13),sparse(13,13));



 Tf{6,2} = interval(sparse(13,13),sparse(13,13));



 Tf{6,3} = interval(sparse(13,13),sparse(13,13));



 Tf{6,4} = interval(sparse(13,13),sparse(13,13));



 Tf{6,5} = interval(sparse(13,13),sparse(13,13));



 Tf{6,6} = interval(sparse(13,13),sparse(13,13));



 Tf{6,7} = interval(sparse(13,13),sparse(13,13));



 Tf{6,8} = interval(sparse(13,13),sparse(13,13));



 Tf{6,9} = interval(sparse(13,13),sparse(13,13));



 Tf{6,10} = interval(sparse(13,13),sparse(13,13));



 Tf{6,11} = interval(sparse(13,13),sparse(13,13));



 Tf{6,12} = interval(sparse(13,13),sparse(13,13));



 Tf{6,13} = interval(sparse(13,13),sparse(13,13));



 Tf{7,1} = interval(sparse(13,13),sparse(13,13));



 Tf{7,2} = interval(sparse(13,13),sparse(13,13));



 Tf{7,3} = interval(sparse(13,13),sparse(13,13));



 Tf{7,4} = interval(sparse(13,13),sparse(13,13));



 Tf{7,5} = interval(sparse(13,13),sparse(13,13));



 Tf{7,6} = interval(sparse(13,13),sparse(13,13));



 Tf{7,7} = interval(sparse(13,13),sparse(13,13));



 Tf{7,8} = interval(sparse(13,13),sparse(13,13));



 Tf{7,9} = interval(sparse(13,13),sparse(13,13));



 Tf{7,10} = interval(sparse(13,13),sparse(13,13));



 Tf{7,11} = interval(sparse(13,13),sparse(13,13));



 Tf{7,12} = interval(sparse(13,13),sparse(13,13));



 Tf{7,13} = interval(sparse(13,13),sparse(13,13));



 Tf{8,1} = interval(sparse(13,13),sparse(13,13));



 Tf{8,2} = interval(sparse(13,13),sparse(13,13));



 Tf{8,3} = interval(sparse(13,13),sparse(13,13));



 Tf{8,4} = interval(sparse(13,13),sparse(13,13));



 Tf{8,5} = interval(sparse(13,13),sparse(13,13));



 Tf{8,6} = interval(sparse(13,13),sparse(13,13));



 Tf{8,7} = interval(sparse(13,13),sparse(13,13));



 Tf{8,8} = interval(sparse(13,13),sparse(13,13));



 Tf{8,9} = interval(sparse(13,13),sparse(13,13));



 Tf{8,10} = interval(sparse(13,13),sparse(13,13));



 Tf{8,11} = interval(sparse(13,13),sparse(13,13));



 Tf{8,12} = interval(sparse(13,13),sparse(13,13));



 Tf{8,13} = interval(sparse(13,13),sparse(13,13));



 Tf{9,1} = interval(sparse(13,13),sparse(13,13));



 Tf{9,2} = interval(sparse(13,13),sparse(13,13));



 Tf{9,3} = interval(sparse(13,13),sparse(13,13));



 Tf{9,4} = interval(sparse(13,13),sparse(13,13));



 Tf{9,5} = interval(sparse(13,13),sparse(13,13));



 Tf{9,6} = interval(sparse(13,13),sparse(13,13));



 Tf{9,7} = interval(sparse(13,13),sparse(13,13));



 Tf{9,8} = interval(sparse(13,13),sparse(13,13));



 Tf{9,9} = interval(sparse(13,13),sparse(13,13));



 Tf{9,10} = interval(sparse(13,13),sparse(13,13));



 Tf{9,11} = interval(sparse(13,13),sparse(13,13));



 Tf{9,12} = interval(sparse(13,13),sparse(13,13));



 Tf{9,13} = interval(sparse(13,13),sparse(13,13));



 Tf{10,1} = interval(sparse(13,13),sparse(13,13));



 Tf{10,2} = interval(sparse(13,13),sparse(13,13));



 Tf{10,3} = interval(sparse(13,13),sparse(13,13));



 Tf{10,4} = interval(sparse(13,13),sparse(13,13));



 Tf{10,5} = interval(sparse(13,13),sparse(13,13));



 Tf{10,6} = interval(sparse(13,13),sparse(13,13));



 Tf{10,7} = interval(sparse(13,13),sparse(13,13));



 Tf{10,8} = interval(sparse(13,13),sparse(13,13));



 Tf{10,9} = interval(sparse(13,13),sparse(13,13));



 Tf{10,10} = interval(sparse(13,13),sparse(13,13));



 Tf{10,11} = interval(sparse(13,13),sparse(13,13));



 Tf{10,12} = interval(sparse(13,13),sparse(13,13));



 Tf{10,13} = interval(sparse(13,13),sparse(13,13));



 Tf{11,1} = interval(sparse(13,13),sparse(13,13));



 Tf{11,2} = interval(sparse(13,13),sparse(13,13));



 Tf{11,3} = interval(sparse(13,13),sparse(13,13));



 Tf{11,4} = interval(sparse(13,13),sparse(13,13));



 Tf{11,5} = interval(sparse(13,13),sparse(13,13));



 Tf{11,6} = interval(sparse(13,13),sparse(13,13));



 Tf{11,7} = interval(sparse(13,13),sparse(13,13));



 Tf{11,8} = interval(sparse(13,13),sparse(13,13));



 Tf{11,9} = interval(sparse(13,13),sparse(13,13));



 Tf{11,10} = interval(sparse(13,13),sparse(13,13));



 Tf{11,11} = interval(sparse(13,13),sparse(13,13));



 Tf{11,12} = interval(sparse(13,13),sparse(13,13));



 Tf{11,13} = interval(sparse(13,13),sparse(13,13));



 Tf{12,1} = interval(sparse(13,13),sparse(13,13));



 Tf{12,2} = interval(sparse(13,13),sparse(13,13));



 Tf{12,3} = interval(sparse(13,13),sparse(13,13));



 Tf{12,4} = interval(sparse(13,13),sparse(13,13));



 Tf{12,5} = interval(sparse(13,13),sparse(13,13));



 Tf{12,6} = interval(sparse(13,13),sparse(13,13));



 Tf{12,7} = interval(sparse(13,13),sparse(13,13));



 Tf{12,8} = interval(sparse(13,13),sparse(13,13));



 Tf{12,9} = interval(sparse(13,13),sparse(13,13));



 Tf{12,10} = interval(sparse(13,13),sparse(13,13));



 Tf{12,11} = interval(sparse(13,13),sparse(13,13));



 Tf{12,12} = interval(sparse(13,13),sparse(13,13));



 Tf{12,13} = interval(sparse(13,13),sparse(13,13));

