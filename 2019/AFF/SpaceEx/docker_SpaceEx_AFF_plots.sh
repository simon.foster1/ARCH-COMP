#!/bin/bash 
function graphplot {
	echo $2 ":"
	docker run  -v `pwd`:`pwd` -w `pwd` spaceex -g $1$2.cfg -m $1$3.xml -o $1$2.gen -vl | grep 'Forbidden\|Computing reachable states done after'
	docker run  -v `pwd`:`pwd` -w `pwd` --entrypoint "graph" spaceex -Tpng --bitmap-size 1024x1024 -C -B -q0.5 $1$2.gen > $1$2.png
}

echo "ISS"
echo "Producing plot for ISSF01-ISS01 over time horizon of 20"
graphplot SpaceStation/ ISSF01-ISS01-plot ISSF01-plot
echo "Producing plot for ISSC01-ISS02 over time horizon of 20"
graphplot SpaceStation/ ISSC01-ISS02-plot ISSC01-plot
echo "done."
echo
echo "Spacecraft Rendezvous"
# Plot
echo "Producing plot for SRNA01-SR0_ over time horizon of 120"
graphplot Rendezvous/ SRNA01-plot SRNA01-SR0_
echo "Producing plot for SRA01-SR0_ over time horizon of 120"
graphplot Rendezvous/ SRA01-plot SRA01-SR0_
echo "done."
echo
echo "Building"
# Plot
echo "Producing plot for BLDF01-BDS01 over time horizon of 1"
graphplot Building/ BLDF01-BDS01-plot BLDF01 
echo "Producing plot for BLDF01-BDS01 over time horizon of 20"
graphplot Building/ BLDF01-BDS01-plot-20 BLDF01 
echo "Producing plot for BLDC01-BDS01 over time horizon of 1"
graphplot Building/ BLDC01-BDS01-plot BLDC01 
echo "Producing plot for BLDC01-BDS01 over time horizon of 20"
graphplot Building/ BLDC01-BDS01-plot-20 BLDC01 
echo "done."
echo
echo "Platoon"
# Plot
echo "Producing plot for PLAD01-BND30 over time horizon of 20"
graphplot Platoon/ PLAD01-BND30-plot PLAD01-BND 
echo "done."
echo
echo "Gearbox"
# Plot
echo "Producing plot for GRBX01-MES01 over time horizon of 20"
graphplot Gear/ GRBX01-MES01-plot GRBX01-MES01
echo "done."


