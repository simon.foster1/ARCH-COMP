function res = test_ellipsoid_lplus
% test_ellipsoid_lplus - unit test function of lplus
%
% Syntax:  
%    res = test_ellipsoid_lplus
%
% Inputs:
%    -
%
% Outputs:
%    res - boolean 
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Matthias Althoff
% Written:      26-July-2016
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------
passed = true;
n = ceil(abs(3*randn));
E1 = ellipsoid.generate(false,n);
E2 = ellipsoid.generate(

if passed
    disp('test_ellipsoid_mtimes successful');
else
    disp('test_ellipsoid_mtimes failed');
end
%------------- END OF CODE --------------
