function display(E)
% display - Displays the center and generators of a Ellipsoid
%
% Syntax:  
%    display(E)
%
% Inputs:
%    E - ellipsoid object
%
% Outputs:
%    ---
%
% Example: 
%    E=ellipsoid([1,0;0,1]);
%    display(E);
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:       Matthias Althoff
% Written:      14-September-2006 
% Last update:  22-March-2007
% Last revision:---

%------------- BEGIN CODE --------------

%display id, dimension
%display(E.contSet);

%display center
disp('q: ');
disp(E.q);

%display shape matrix
disp('Q: ');
disp(E.Q); 

%display actual dimension
disp('dimension: ');
disp(E.dim); 

%display whether degenerate or not
disp('degenerate: ');
disp(E.isdegenerate); 
%------------- END OF CODE --------------