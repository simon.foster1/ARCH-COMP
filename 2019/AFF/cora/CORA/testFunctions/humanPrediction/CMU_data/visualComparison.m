function visualComparison(fileName,varargin)
% visualComparison - shows videos of original and corrected CMU data side
% by side
% 
% Syntax:  
%    visualComparison(name,S,varargin)
%
% Inputs:
%    traj - recorded trajectory of a marker
%    correction_flag - 1 if trajectory should be automatically
%
% Outputs:
%    -
%
% Example: 
%
% 
% Author:       Matthias Althoff
% Written:      10-March-2019
% Last update:  ---
% Last revision:---


%------------- BEGIN CODE --------------

% change directory
cd([coraroot,'/testFunctions/humanPrediction/CMU_data/videos']);

% adjust view vector
if nargin>1
    viewVector=varargin{1};
else
    viewVector=[2 -1 3];
end

if nargin > 2
    still = varargin{3};
else
    still = 1;
end

%load origtinal data
load CMU_data_for_Science
ALLMOVEMENTS_orig = ALLMOVEMENTS;

% load data
eval(['load ', fileName])


% set marker style
markerstyle = {'kx', 'kx', 'ko', 'ko',...
    'co', 'cx', 'co', 'cx', 'co', 'co', 'c+',...
    'go', 'gx', 'go', 'gx', 'go', 'go', 'g+', ...
    'r^', 'r^', 'r^', 'r^', 'rx', ...
    'yd', 'yd', 'yd', 'yd', ...
    'bx', 'bo', 'bx', 'bo', 'b+', 'bx', 'bo',...
    'mx', 'mo', 'mx', 'mo', 'm+', 'mx', 'mo',...
    };

% number of movements
nrOfMovements = length(ALLMOVEMENTS(:,1))-1;

%for iMovement = 1:nrOfMovements
for iMovement = 150:nrOfMovements 
%for iMovement = 32:32
    
    % init video object
    %vidObj = VideoWriter([fileName,'.mpeg4'],'MPEG-4'); % only windows or MAC
    vidObj = VideoWriter([fileName,'_',num2str(iMovement)],'Motion JPEG AVI');
    
    % adapt frame rate
    vidObj.FrameRate = ALLMOVEMENTS{iMovement+1,3};
    open(vidObj);

    % extract trajectories of all markers
    fullTraj_orig = ALLMOVEMENTS_orig{iMovement+1,2};
    fullTraj_corr = ALLMOVEMENTS{iMovement+1,2};

    % reshape to position vectors for each marker
    res = reshape(fullTraj_orig(1,:),3,[]); 
    
    % average value of markers
    avgMarkerPos = sum(res,2)/size(res,2); 


    figure
 %   set(gcf,'Position',[0 0 960 540])
    grid off
    %view(viewVector)
    view(120,9);
    %axis equal

    axis([avgMarkerPos(1)-3000,avgMarkerPos(1)+3000,...
        avgMarkerPos(2)-3000,avgMarkerPos(2)+3000,...
        avgMarkerPos(3)-1200,avgMarkerPos(3)+1200]);
 %   axis manual
    
    
    % loopover time steps
    for iStep = 1:size(fullTraj_orig,1)
        
        % clear figure
        clf(gcf);
        
        % plot human original
        subplot(1,2,1);
        hold on
        view(viewVector)
        axis equal
        grid off
        set(gca,'visible','off')
        set(gcf,'Color',[1 1 1]);
        plotHuman(fullTraj_orig(iStep,:));
        
        % plot human corrected
        subplot(1,2,2);
        hold on
        view(viewVector)
        axis equal
        grid off
        set(gca,'visible','off')
        set(gcf,'Color',[1 1 1]);
        plotHuman(fullTraj_corr(iStep,:));
        
        % add frame
        thisframe=getframe(gcf);%,[270 140 480 270]);
        writeVideo(vidObj,thisframe);
    end
    
    % close video
    close(vidObj)
    
    iMovement
end

